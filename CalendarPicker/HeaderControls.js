import React, { useState, useEffect } from "react";
import {
  View,
  Text,
  Platform,
  TouchableOpacity,
  Appearance,
  AppState
} from 'react-native';
import PropTypes from 'prop-types';
import { Utils } from './Utils';
import Controls from './Controls';
import { color } from 'react-native-reanimated';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
export default function HeaderControls(props) {
  const {
    styles,
    currentMonth,
    currentYear,
    onPressNext,
    onPressPrevious,
    onPressMonth,
    onPressYear,
    months,
    previousComponent,
    nextComponent,
    previousTitle,
    nextTitle,
    previousTitleStyle,
    nextTitleStyle,
    textStyle,
    restrictMonthNavigation,
    maxDate,
    minDate,
    headingLevel,
    monthYearHeaderWrapperStyle,
  } = props;
  const MONTHS = months || Utils.MONTHS; // English Month Array
  const monthName = MONTHS[currentMonth];
  const year = currentYear;

  const disablePreviousMonth = restrictMonthNavigation && Utils.isSameMonthAndYear(minDate, currentMonth, currentYear);
  const disableNextMonth = restrictMonthNavigation && Utils.isSameMonthAndYear(maxDate, currentMonth, currentYear);

  const accessibilityProps = { accessibilityRole: 'header' };
  if (Platform.OS === 'web') {
    accessibilityProps['aria-level'] = headingLevel;
  }

  const [dark, setDark] = useState(false);
  // console.log("AlsMagazine", data);
  /** function get the current theme of app **/
  const setTheme = () => {
    let isDark = Appearance.getColorScheme() === "dark" ? true : false;
    setDark(isDark);
  };

  useEffect(() => {
    //calling to get the current state of theme
    setTheme();

    //adding listener
    AppState.addEventListener("change", handleAppStateChange);

    //removing listner
    return () => {
      AppState.removeEventListener("change", handleAppStateChange);
    };
  });

  /** call back functoin  **/
  const handleAppStateChange = async (state: any) => {
    setTheme();
  };

  return (
    <View style={styles.headerWrapper}>
      <Controls
        disabled={disablePreviousMonth}
        label={previousTitle}
        component={previousComponent}
        onPressControl={onPressPrevious}
        styles={styles.previousContainer}
        textStyles={[styles.navButtonText, textStyle, previousTitleStyle]}
      />
      <View style={[styles.monthYearHeaderWrapper,monthYearHeaderWrapperStyle]}>
        <TouchableOpacity onPress={onPressMonth}>
        <Text style={[styles.monthHeaderMainText, ],{color:dark?'#cadfff':'#004990',fontSize:wp('3.73%'),fontWeight:'bold',fontFamily: "BootsSharp-Regular",}} {...accessibilityProps} >
            { monthName }
          </Text>
        </TouchableOpacity>
        {/* <TouchableOpacity onPress={onPressYear}>
          <Text style={[styles.yearHeaderMainText, textStyle]}>
            { year }
          </Text>
        </TouchableOpacity> */}
      </View>
      <Controls
        disabled={disableNextMonth}
        label={nextTitle}
        component={nextComponent}
        onPressControl={onPressNext}
        styles={styles.nextContainer}
        textStyles={[styles.navButtonText, textStyle, nextTitleStyle]}
      />
    </View>
  );
}

HeaderControls.propTypes = {
  currentMonth: PropTypes.number,
  currentYear: PropTypes.number,
  onPressNext: PropTypes.func,
  onPressPrevious: PropTypes.func,
  onPressMonth: PropTypes.func,
  onPressYear: PropTypes.func,
};
